these scripts are used to process sweep_out files genterated during sdz
uncertainty runs
scripts generate plots, shapefiles etc
they should be run inteactivelly
scripts rely on tidyverse and opther packages

they can be run in following order:
1. output_processing_sdz2.R - read csv, filter out zones, convert to "long
format
2. output_processing_sdz2.cont.R calculate flow difference
3. sdz_plot.R various plots
4. maps and shapefiles
5. optional file for including all zones in processing, produces long format
(not that this has not been processed furter, could be done using scripts 2:3,
but may require some tweaking of scripts, and these would be huge files using
 tens of GB of RAM, so it may be tricki.

note that scripts may need to be tweaked to change saving location for files,
and these can be very large (many GB)

all large input and output files have been saved remotly on a separate server
\\fileenviro\Esci\Heretaunga Model Big Files\hmp035\sdz uncertainty



