#script by Pawel Rakowski
#5/03/2018
#this script is used to process sweep_out.csv file
#to be used in an interactive model


library(tidyverse)
library(stringr)
library(lubridate)
install.packages("lubridate")

sdz_out <- read_csv("sweep_out_1.csv") 

runs <- sdz_out %>% 
  select(run_id,input_run_id) %>% 
  mutate(input_run_id1 = gsub("_base","_0",input_run_id)) # this is to get base run (original parameter file before uncertainty runs)
#parse run data to columns
runs_base <- runs %>%
  filter(substr(input_run_id1,1,4) == "base") %>% 
  separate(input_run_id1,c("sim_type","realisation"),remove=F) %>% 
  mutate(realisation = as.numeric(realisation)) %>% 
  mutate(L=NA,R=NA,C=NA) %>% 
  mutate_at(c("L","R","C"),as.numeric) %>% 
  select(run_id,sim_type,L,R,C,realisation,input_run_id)


runs_well <- runs %>% 
  filter(substr(input_run_id1,1,4) != "base")%>% 
  separate(input_run_id1,c("sim_type","L","R","C","realisation"),remove=F) %>% 
  mutate_at(c("L","R","C","realisation"),as.numeric) %>% 
  select(run_id,sim_type,L,R,C,realisation,input_run_id)
  

runs <- bind_rows(runs_base,runs_well) %>% 
  mutate(well_id = L*1000*1000+R*1000+C) 

sdz_out2 <- left_join(runs,sdz_out)

rm(runs_base,runs_well)


#get dates vector to match time index from the sample file  
sample <- read_tsv("flow_comb.smp",col_names = F,col_types = cols(X2 = col_character(),
                                                                  X3 = col_character() )  ) 
dates <-  sample %>% 
  distinct(X2,X3) %>% 
  mutate(date_time = parse_date_time(paste(X2,X3),"ymd HMS")) %>% 
  select(date_time) %>% 
  mutate(time_ind = row_number()-1) %>% 
  mutate(sim_time = as.numeric(date_time - floor_date(min(date_time),"days"),"days"))


#creat a long fomrat data and parse obs types and time indexes
sdz_out_lng <- sdz_out2 %>% 
  gather("obs","value",starts_with("SDZ")) %>% 
  mutate(obs = gsub("Z_Z_","Z_Z-",obs)) %>% # this is needed to separate columns
  separate(obs,c("sim","zone","time_ind"),"_",remove = F) %>% 
  mutate(time_ind = as.numeric(time_ind)) %>% 
  mutate(obs_type = if_else(substr(zone,1,2)=="Z-","catchm","stream_gr")) %>% 
  left_join(dates,by ="time_ind")

#get list of all wells used
well_list <- sdz_out_lng %>% 
  distinct(well_id)

#get all observations
obs_list <- distinct(sdz_out_lng,obs,.keep_all =T) 

#get list of streams 
streams_list <-  sdz_out_lng %>% 
  distinct(zone)

#calculate stream depletion as differece by arraning by well no(if 0 means not pumping, and then grouping and getting first value for the difference)
sdz_out_lng1 <- sdz_out_lng %>% 
  arrange(sim_type,well_id,realisation,zone,time_ind) %>% 
  group_by(realisation,zone,time_ind) %>% 
  mutate(value_diff = value -first(value),
         value_zero = first(value)) %>% 
  arrange(realisation,zone,time_ind) %>% 
  filter(sim_type != "base")

#filter out to get grouped results only
sdz_out_lng1_stream <- sdz_out_lng1 %>% 
  filter(obs_type == "stream_gr") 

# filter out to get one well and one stream only for plotting and testing
sdz_out_lng1_sel <- sdz_out_lng1_stream %>%
  filter(well_id %in% c(1192336),zone == "TTK.W") #%>%
 #filter(realisation %in% c( 0))
#plot one location all realisations
chart <- ggplot( sdz_out_lng1_sel,aes(sim_time,value_diff,col = factor(realisation)))+
  geom_line()
chart

#creat sample for testing 
#
# sdz_out_lng_sample <- sdz_out_lng %>% 
#   filter(well_id %in% c(1192336,1187336,0)) %>% 
#   filter(zone %in% c("NGRRR","MNGTR")) %>% 
#   filter(realisation %in% c(99,100)) #%>% 
#   #filter(time_ind == 15)# %>% 
#  #
# select(well_id,realisation,zone,value,time_ind)


#filter out to get last TS and base realisation in one location for testing
#  sdz_out_lng1_sel0 <-  sdz_out_lng1 %>% 
#   filter(well_id %in% c(1192336)) %>% 
#   filter(realisation == 0) %>% 
#   filter(time_ind == 49) %>% 
#   filter(sim_type == "wf") %>% 
#   select(zone,value_diff)
# sdz_out_lng1_sel0

#subset for checking, all realistiaons in for one stream and one time 
# sdz_out_lng1_sel2 <- sdz_out_lng1_sel %>% 
#   filter(time_ind == 15) #%>% 
#  # filter(zone == "MNGTR")



  

