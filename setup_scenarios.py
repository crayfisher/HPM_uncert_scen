"""manual mods
changed models to all use the same DRN and GHB files
extended drain.tpl to have extra stress period repeats
"""

import os
import sys
import shutil
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import flopy
import pyemu
"""Lower left of the grid:
1891460 5594400
 
M3 simultation start:
1/07/1980"""
xll,yll = 1891460, 5594400
proj4_str = "EPSG:2193"
start_datetime = "1-July-1980"



org_pst = "hpm035res.pst"
org_d = "hpm035_res_sel"


def setup_base_case_pst(new_d,skip_rm_exts=[]):
    shutil.copy2(os.path.join(org_d,"tabl4.dat"),os.path.join(new_d,"tabl4.dat"))
    shutil.copy2(os.path.join(org_d, "source_modflow_zones4.csv"),
                 os.path.join(new_d, "source_modflow_zones4.csv"))

    pst = pyemu.Pst(os.path.join(org_d,org_pst))

    template_files = ["drain.tpl", "ghb.tpl"]
    template_files.extend([t for t in pst.template_files if "point" in t.lower()])
    keep_tpl_files = [t for t in pst.template_files if t in template_files]
    keep_in_files = []
    for t in keep_tpl_files:
        keep_in_files.append(pst.input_files[pst.template_files.index(t)])

    pst.template_files = keep_tpl_files
    pst.input_files = keep_in_files
    #input_files = [pst.input_files[]]
    pst.instruction_files = []
    pst.output_files = []

    # copy over tpl files
    for tpl_file in pst.template_files:
        shutil.copy2(os.path.join(org_d,tpl_file),os.path.join(new_d,tpl_file))

    # copy over ins files
    for ins_file in pst.instruction_files:
        shutil.copy2(os.path.join(org_d, ins_file), os.path.join(new_d, ins_file))

    # copy over need "in" files
    # in_files = [f for f in os.listdir(org_d) if "fac2r" in f and f.endswith(".in")]
    # for in_file in in_files:
    #     shutil.copy(os.path.join(org_d,in_file),os.path.join(new_d,in_file))
    # remove all ins files - these are scenario specific


    pst.observation_data.drop(pst.observation_data.index,inplace=True)
    #turnoff PI
    pst.prior_information = pst.null_prior

    pst.control_data.noptmax = 0

    # add "dummy" tpl file for R preprocessors
    par_file = "hpm035res.par"
    shutil.copy2(os.path.join(org_d,par_file),os.path.join(new_d,par_file))
    f_in = open(os.path.join(new_d,par_file),'r')
    f_tpl = open(os.path.join(new_d,par_file+".tpl"),'w')
    f_tpl.write("ptf ~\n")
    f_tpl.write(f_in.readline())
    for line in f_in:
        raw = line.strip().split()
        raw[1] = "~    {0}     ~".format(raw[0])
        f_tpl.write(' '.join(raw)+'\n')
    f_in.close()
    f_tpl.close()
    pst.template_files.append(par_file+".tpl")
    pst.input_files.append(par_file)

    frun_commands = []
    #files to remove
    rm_cmds = []
    rm_exts = [".cbb",".cre",".wel",".riv","._kx","._kz","._ss","._sy"]
    for skip in skip_rm_exts:
        if skip in rm_exts:
            rm_exts.remove(skip)
    with open(os.path.join(new_d,"remover.py"),'w') as f:
        f.write("import os\n")
        for rm_ext in rm_exts:
            f.write("[os.remove(f) for f in os.listdir('.') if f.lower().endswith('{0}')]\n".
                    format(rm_ext))
    frun_commands.append("python remover.py")


    # add f2r commands
    in_files = [f for f in os.listdir(org_d) if "fac2r" in f and f.endswith(".in")]
    for in_file in in_files:
        shutil.copy2(os.path.join(org_d, in_file), os.path.join(new_d, in_file))
        with open(os.path.join(new_d, in_file),'r') as f:
            for line in f:
                line = line.strip().split()[0]
                fi = os.path.join(org_d,line)
                if os.path.exists(fi):
                    shutil.copy(fi,os.path.join(new_d,line))
        frun_commands.append("fac2real < {0} > {0}.out".format(in_file))

    pst.control_data.pestmode = "estimation"
    pst.model_command = "python forward_run.py"
    return pst,frun_commands



# def setup_m3():
#
#     new_d = "m3"
#     base_name = "HPM_M3"
#     no_name = "HPM_M3_zero"
#     m = flopy.modflow.Modflow.load("HPM_M3.nam",check=False,verbose=True,forgive=False,model_ws=org_d)
#     m.change_model_ws(new_d,reset_external=True)
#     m.sr.xll = xll
#     m.sr.yll = yll
#     # super hack alert
#     ext_fnames = []
#     for ef in m.external_fnames:
#         ext_fnames.append(base_name+'.'+ef.split(".")[-1])
#     m.external_fnames = ext_fnames
#
#     m.start_datetime = start_datetime
#
#     #m.rch.rech.fmtin = "(BINARY)"
#
#     #m.drn.stress_period_data.reset_binary(True)
#     #m.external_path = '.'
#     m.name = base_name
#     m.write_input()
#
#     f_in = open(os.path.join(new_d,base_name+".nam"))
#     f_out = open(os.path.join(new_d,no_name+".nam"),'w')
#     for line in f_in:
#         if "wel" in line:
#             line = "#" + line
#         if line.upper().startswith("DATA(BINARY)"):
#             line = line.replace(base_name,no_name)
#         f_out.write(line)
#     f_in.close()
#     f_out.close()
#
#     #get stuff for mod2obs
#     in_file = "head_mod2obs_m3.in"
#     shutil.copy2(os.path.join(org_d,in_file),os.path.join(new_d,in_file))
#     f_in_base = open(os.path.join(new_d,"mod2obs_base.in"),'w')
#     f_in_nopump = open(os.path.join(new_d,"mod2obs_nopump.in"),'w')
#
#     with open(os.path.join(new_d,in_file),'r') as f:
#         for line in f:
#             # if "hds" in line
#             fi = os.path.join(org_d,line.strip())
#             if os.path.exists(fi):
#                 shutil.copy(fi,os.path.join(new_d,line.strip()))
#
#     f_in_base.close()
#     f_in_nopump.close()
#     frun_commands = ["Rscript create_m3_riv.r", "Rscript create_m3_wel.r", ]
#     # add f2r commands
#     in_files = [f for f in os.listdir(org_d) if "fac2r" in f and f.endswith(".in")]
#     for in_file in in_files:
#         shutil.copy2(os.path.join(org_d,in_file),os.path.join(new_d,in_file))
#         frun_commands.append("fac2real < {0} > {0}.out".format(in_file))
#
#     frun_commands.append("mf2005 {0}.nam > {0}.stdout".format(base_name))
#     frun_commands.append("mf2005 {0}.nam >{0}.stdout".format(no_name))
#
#     pst = setup_base_case_pst(new_d)
#     pst.write(os.path.join(new_d,new_d+".pst"))

def setup_scenario_model_files(nam_files,new_d,oorg_d=None):
    if not isinstance(nam_files,list):
        nam_files = [nam_files]

    if os.path.exists(new_d):
        shutil.rmtree(new_d)
    os.mkdir(new_d)
    for nam_file in nam_files:
        copy_with_nam_file(nam_file,new_d,oorg_d=oorg_d)

def copy_with_nam_file(nam_file,new_d,oorg_d=None):
    copy_file(nam_file,new_d,oorg_d=oorg_d)
    with open(os.path.join(new_d,nam_file),'r') as f:
        for line in f:
            if "data" in line.lower() or "lst" in line:
                continue
            fname = line.strip().split()[2]
            copy_file(fname,new_d,oorg_d=oorg_d)
            process_model_file(fname,new_d,oorg_d=oorg_d)

def process_model_file(fname,new_d,oorg_d=None):
    if oorg_d is None:
        oorg_d = org_d
    print("processing file ",fname)
    with open(os.path.join(oorg_d,fname),'r') as f:
        for line in f:
            if line.strip().lower().startswith("open"):
                copy_file(line.strip().split()[1],new_d,oorg_d=oorg_d)

def copy_file(fname,new_d,new_fname=None,oorg_d=None):
    if new_fname is None:
        new_fname = fname
    if oorg_d is None:
        oorg_d = org_d
    if not os.path.exists(os.path.join(oorg_d,fname)):
        print("file doesn't exists: ",os.path.join(oorg_d,fname))

    else:
        shutil.copy2(os.path.join(oorg_d,fname),os.path.join(new_d,new_fname))
        return os.path.join(new_d,new_fname)

def setup_m3():
    new_d = "m3"
    if os.path.exists(new_d):
        shutil.rmtree(new_d)
    os.mkdir(new_d)
    nam_files = ["HPM_M3.nam", "HPM_M3_zero.nam"]
    smp_files = ["hsim_m3.smp","hsim_m3_zero.smp","flow.smp","flow_comb.smp"]
    #smp_files = ["hsim_m3.smp", "hsim_m3_zero.smp"]

    setup_scenario_model_files(nam_files, new_d)
    template_files = ["drain.tpl","ghb.tpl"]

    pst,frun_commands = setup_base_case_pst(new_d)

    #write rch tpl file
    rch_file = [f for f in os.listdir(new_d) if f.lower().endswith(".rch")][0]
    f_rch = open(os.path.join(new_d,rch_file),'r')
    with open(os.path.join(new_d,rch_file+".tpl"),'w') as f:
        f.write("ptf ~\n")
        for line in f_rch:
            raw = line.strip().split()
            if raw[0].lower().startswith("open"):
                raw[2] = "~     Rch    ~"
            f.write(' '.join(raw)+'\n')
    os.chdir(new_d)
    pst.add_parameters(rch_file+".tpl",rch_file)
    os.chdir("..")


    # copy over R needs
    r_d = "M3_scripts"
    [shutil.copy2(os.path.join(r_d, f), os.path.join(new_d, f)) for f in os.listdir(r_d)]

    frun_commands.extend(["Rscript --default-packages=methods create_m3_riv.R",
                          "Rscript --default-packages=methods create_m3_wel.R", ])

    for f in nam_files:
        frun_commands.append("mf2005 {0} > {0}.stdout".format(f))

    # setup obs for the head smp files - using the hsim_m3.smp as the base case
    #base_smp_file = "hobs_m3.smp"
    for smp_file in smp_files:
        print(smp_file)
        #smp_file = copy_file(base_smp_file,new_d,smp_file)
        prefix = 'm3_'
        if "zero" in smp_file:
            prefix = "m3z_"
        date_fmt = None
        use_generic = False
        gw_utils = True
        if "flow" in smp_file.lower():
            date_fmt = "%Y-%m-%d %H:%M:%S"
            use_generic = True
            gw_utils = False
        pyemu.pst_utils.smp_to_ins(os.path.join(new_d,smp_file),prefix=prefix,
                                   use_generic_names=use_generic,
                                   datetime_format=date_fmt,
                                   gwutils_compliant=gw_utils)

    # get stuff for mod2obs
    in_base = "head_mod2obs_m3.in"
    in_zero = "head_mod2obs_m3_zero.in"
    frun_commands.append("mod2obs <{0} >{0}.stdout".format(in_base))
    frun_commands.append("mod2obs <{0} >{0}.stdout".format(in_zero))
    frun_commands.append("bud2hyd <bud2hyd_M3_zero.in ")
    frun_commands.append("bud2hyd <bud2hyd_M3.in")
    frun_commands.append("Rscript --default-packages=methods process_flow.R")
    for in_file in [in_base,in_zero]:
        with open(os.path.join(new_d, in_file), 'r') as f:
            for line in f:
                fi_org = os.path.join(org_d, line.strip())
                fi_new = os.path.join(new_d,line.strip())
                if os.path.exists(fi_org) and not os.path.exists(fi_new):
                    shutil.copy(fi_org, fi_new)

    os.chdir(new_d)
    for smp_file in smp_files:
        pst.add_observations(smp_file+".ins",smp_file,inschek=False)
    os.chdir("..")

    pst.write(os.path.join(new_d,new_d+".pst"))

    with open(os.path.join(new_d,"forward_run.py"),'w') as f:
        f.write("import os\n")
        for c in frun_commands:
            f.write("os.system('{0}')\n".format(c))

    #pyemu.helpers.run("pestchek {0}.pst".format(new_d),cwd=new_d)

def test(new_d):
    test_d = new_d+"_test"
    if os.path.exists(test_d):
        shutil.rmtree(test_d)

    shutil.copytree(new_d,test_d)
    pyemu.helpers.run("pestpp {0}.pst".format(new_d),cwd=test_d)

def setup_m4():
    nam_files_lst = [["HPM_M4.nam"],["HPM_M4_incr.nam"]]
    new_d_lst = ["m4","m4_incr"]
    smp_files_lst = [["hsim_m4.smp","flow.smp","flow_comb.smp"],["hsim_m4_incr.smp","flow.smp","flow_comb.smp"]]
    m2o_in_file_lst = ["head_mod2obs_m4.in","head_mod2obs_m4_incr.in"]
    b2h_in_file_lst = ["bud2hyd_M4.in","bud2hyd_M4_incr.in"]
    rscript_lst = ["process_flow_M4.R","process_flow_M4_incr.R"]
    r_d = "M4_scripts"

    for nam_files, new_d, smp_files, m2o_in_file, b2h_in_file, rscript in \
            zip(nam_files_lst,new_d_lst,
                smp_files_lst,m2o_in_file_lst,b2h_in_file_lst, rscript_lst):

        if os.path.exists(new_d):
            shutil.rmtree(new_d)
        os.mkdir(new_d)

        setup_scenario_model_files(nam_files,new_d)
        pst,frun_commands = setup_base_case_pst(new_d,skip_rm_exts=[".wel"])

        # write rch tpl file
        rch_file = [f for f in os.listdir(new_d) if f.lower().endswith(".rch")][0]
        f_rch = open(os.path.join(new_d, rch_file), 'r')
        with open(os.path.join(new_d, rch_file + ".tpl"), 'w') as f:
            f.write("ptf ~\n")
            for line in f_rch:
                raw = line.strip().split()
                if raw[0].lower().startswith("open"):
                    raw[2] = "~     Rch    ~"
                f.write(' '.join(raw) + '\n')
        os.chdir(new_d)
        pst.add_parameters(rch_file + ".tpl", rch_file)
        os.chdir("..")

        # copy over R needs
        [shutil.copy2(os.path.join(r_d, f), os.path.join(new_d, f)) for f in os.listdir(r_d)]

        frun_commands.append("Rscript --default-packages=methods create_m4_riv.R")


        for f in nam_files:
            frun_commands.append("mf2005 {0} > {0}.stdout".format(f))

        # setup obs for the smp files - using the hsim_smp as the base case
        for smp_file in smp_files:
            print(smp_file)
            # smp_file = copy_file(base_smp_file,new_d,smp_file)
            prefix = 'm4_'
            if "zero" in smp_file:
                prefix = "m3z_"
            date_fmt = None
            use_generic = False
            gw_utils = True
            if "flow" in smp_file.lower():
                date_fmt = "%Y-%m-%d %H:%M:%S"
                use_generic = True
                gw_utils = False
            pyemu.pst_utils.smp_to_ins(os.path.join(new_d, smp_file), prefix=prefix,
                                       use_generic_names=use_generic,
                                       datetime_format=date_fmt,
                                       gwutils_compliant=gw_utils)
        # get stuff for mod2obs
        frun_commands.append("mod2obs <{0} >{0}.stdout".format(m2o_in_file))
        with open(os.path.join(new_d, m2o_in_file), 'r') as f:
            for line in f:
                fi = os.path.join(org_d, line.strip())
                if os.path.exists(fi):
                    shutil.copy(fi, os.path.join(new_d, line.strip()))
        frun_commands.append("bud2hyd <{0} >{0}.stdout".format(b2h_in_file))
        frun_commands.append("Rscript --default-packages=methods {0}".format(rscript))

        os.chdir(new_d)
        for smp_file in smp_files:
            pst.add_observations(smp_file+".ins",smp_file,inschek=False)
        os.chdir("..")
        pst.write(os.path.join(new_d,new_d+".pst"))

        with open(os.path.join(new_d,"forward_run.py"),'w') as f:
            f.write("import os\n")
            for c in frun_commands:
                f.write("os.system('{0}')\n".format(c))

        #pyemu.helpers.run("pestchek {0}.pst".format(new_d),cwd=new_d)


def setup_sdz():
    new_d = "sdz"
    oorg_d = "sdz_base"
    r_d = "SDZ_scripts"
    if os.path.exists(new_d):
        shutil.rmtree(new_d)
    os.mkdir(new_d)
    nam_files = ['HPM_SDZ_sim.nam']
    b2h_in_file = "bud2hyd_sdz_sim.in"
    smp_files = ["flow.smp","flow_comb.smp"]
    setup_scenario_model_files(nam_files, new_d,oorg_d=oorg_d)
    pst, frun_commands = setup_base_case_pst(new_d,skip_rm_exts=[".wel"])

    # copy over R needs
    [shutil.copy2(os.path.join(oorg_d, f), os.path.join(new_d, f)) for f in os.listdir(oorg_d)]
    [shutil.copy2(os.path.join(r_d, f), os.path.join(new_d, f)) for f in os.listdir(r_d)]

    # setup obs for the smp files - using the hsim_smp as the base case
    for smp_file in smp_files:
        print(smp_file)
        # smp_file = copy_file(base_smp_file,new_d,smp_file)
        prefix = 'sdz_'
        date_fmt = None
        use_generic = False
        gw_utils = True
        if "flow" in smp_file.lower():
            date_fmt = "%Y-%m-%d %H:%M:%S"
            use_generic = True
            gw_utils = False
        pyemu.pst_utils.smp_to_ins(os.path.join(new_d, smp_file), prefix=prefix,
                                   use_generic_names=use_generic,
                                   datetime_format=date_fmt,
                                   gwutils_compliant=gw_utils)

    frun_commands.append("Rscript --default-packages=methods create_sdz_riv.R")

    for f in nam_files:
        frun_commands.append("mf2005 {0} > {0}.stdout".format(f))

    frun_commands.append("bud2hyd <{0} >{0}.stdout".format(b2h_in_file))

    frun_commands.append("Rscript --default-packages=methods process_sdz.r")

    # write wel tpl
    tpl_file,in_file = prep_sdz_wel(new_d,nam_files[0])
    os.chdir(new_d)
    pst.add_parameters(tpl_file,in_file)
    os.chdir("..")
    par = pst.parameter_data
    wf_pars = par.loc[par.parnme.apply(lambda x: x.startswith("wf")),"parnme"]

    par.loc[wf_pars,"partrans"] = "none"
    par.loc[wf_pars,"parval1"] = 0.0
    par.loc[wf_pars,"parubnd"] = 1.0e+10
    par.loc[wf_pars,"parlbnd"] = -1.0e+10

    os.chdir(new_d)
    for smp_file in smp_files:
        pst.add_observations(smp_file + ".ins", smp_file, inschek=False)
    os.chdir("..")
    pst.write(os.path.join(new_d, new_d + ".pst"))

    with open(os.path.join(new_d, "forward_run.py"), 'w') as f:
        f.write("import os\n")
        for c in frun_commands:
            f.write("os.system('{0}')\n".format(c))

    build_sdz_ensemble()


def lrc(x):
        return "{0:1.0f}_{1:03.0f}_{2:03.0f}".format(x.l,x.r,x.c)

def prep_sdz_wel(new_d,nam_file):

    # load the wel list
    wel_df = pd.read_csv(os.path.join(new_d,"well_list_sf_sel_all.csv"))
    wel_df.columns = wel_df.columns.map(str.lower)
    wel_df.loc[:,"lrc"] = wel_df.apply(lambda x: lrc(x),axis=1)
    wel_df.loc[:,"flux"] = 0.0

    m = flopy.modflow.Modflow.load(nam_file,model_ws=new_d,check=False,load_only=["wel"],verbose=True)
    in_file = m.wel.file_name[0]
    tpl_file = in_file+".tpl"

    sp_df = pd.DataFrame.from_records(m.wel.stress_period_data[0])
    for ii,oo in zip(['k','i','j'],['l','r','c']):
        sp_df.loc[:,oo] = sp_df.loc[:,ii] + 1
    print(sp_df.shape,wel_df.shape)
    sp_df_val = sp_df.append(wel_df)
    print(sp_df_val.shape)
    wel_df.loc[:,"flux"] = wel_df.lrc.apply(lambda x: "~  wf_{0}   ~".format(x))
    sp_df_tpl = sp_df.append(wel_df)
    print(sp_df_tpl.shape)

    with open(os.path.join(new_d,tpl_file),'w') as f:
        f.write("ptf ~\n")
        f.write("# wel file \n")
        f.write("{0}  0 NOPRINT\n".format(sp_df_tpl.shape[0]))
        f.write("{0}  0\n".format(sp_df_tpl.shape[0]))
        sp_df_tpl.loc[:,['l','r','c','flux']].to_csv(f,sep=' ',header=False,index=False,quotechar=' ')
        for kper in range(10000):
            f.write("-1 0 NOPRINT\n")

    return tpl_file,in_file


def extract_stress_periods(m,kper_start,kper_end):
    """todo: new strt from head file?
    set spatial ref
    set start datetime
    oc
    """
    assert kper_start < kper_end
    assert kper_start > 0 and kper_start < m.nper-1
    assert kper_end > 1 and kper_end < m.nper

    m_new = flopy.modflow.Modflow(m.name+"_new",version=m.version,exe_name=m.exe_name,
                                  model_ws = m.name+"_new")

    #spatial reference

    #start datetime - this one is tricky

    # dis
    perlen = m.dis.perlen.array[kper_start:kper_end]
    nstp = m.dis.nstp.array[kper_start:kper_end]
    steady = m.dis.steady.array[kper_start:kper_end]
    tsmult = m.dis.tsmult.array[kper_start:kper_end]
    flopy.modflow.ModflowDis(m_new,nlay=m.nlay,nrow=m.nrow,ncol=m.ncol,
                             nper=perlen.shape[0],perlen=perlen,nstp=nstp,
                             botm=m.dis.botm,top=m.dis.top,delr=m.dis.delr,
                             delc=m.dis.delc,tsmult=tsmult,steady=steady)

    flopy.modflow.ModflowBas(m_new,ibound=m.bas6.ibound,strt=m.bas6.strt,hnoflo=m.bas6.hnoflo)

    paks = ["DIS","BAS6"]

    if m.lpf is not None:
        lpf = m.lpf
        flopy.modflow.ModflowLpf(m_new,hk=lpf.hk,vka=lpf.vka,laytyp=lpf.laytyp,
                                 layavg=lpf.layavg,chani=lpf.chani,layvka=lpf.layvka,
                                 ss=lpf.ss,sy=lpf.sy,hdry=lpf.hdry,laywet=lpf.laywet,
                                 vkcb=lpf.vkcb,wetdry=lpf.wetdry,ipakcb=lpf.ipakcb)
        paks.append("LPF")

    if m.rch is not None:
        rch = m.rch
        rech = rch.rech.array[kper_start:kper_end,:,:]
        flopy.modflow.ModflowRch(m_new,nrchop=rch.nrchop,ipakcb=rch.ipakcb,rech=rech,irch=rch.irch)
        paks.append("RCH")


    oc_spd = {}
    items = ["save head","save budget"]
    for kper,nnstp in enumerate(nstp):
        oc_spd.update({(kper,kstp):items for kstp in range(nnstp)})
    flopy.modflow.ModflowOc(m_new,stress_period_data=oc_spd)

    # most BC packages - not SFR for sure
    for pak in m.packagelist:
        name = pak.name[0]
        if name in ["DIS","BAS6"]:
            continue
        if name.lower() == "oc":
            pass
        elif hasattr(pak,"stress_period_data"):

            # for kper in range(kper_start,kper_end):
            #     ra = pak.stress_period_data[kper]
            #     print(ra.shape)
            print("processing ",name)
            new_spd = {kper:pak.stress_period_data[kper] for kper in range(kper_start,kper_end)}
            pak_constructor = m_new.mfnam_packages[name.lower()]
            pak_constructor(m_new,stress_period_data=new_spd,ipakcb=pak.ipakcb)
            paks.append(name.upper())
        else:
            if name.lower() in ['gmg','nwt','pcg']:
                pak_constructor = m_new.mfnam_packages[name.lower()]
                new_pak = pak_constructor(m_new)
                for attr in dir(pak):
                    val = getattr(pak,attr)
                    try:
                        setattr(new_pak,attr,val)
                    except:
                        print("couldn't set {0} for {1}".format(attr,name))
            paks.append(name.upper())


    return m_new


def test_extract():
    m = flopy.modflow.Modflow.load("HPM_SDZ_sim.nam",model_ws="sdz_base",verbose=True,check=False)

    m_new = extract_stress_periods(m,1,3)
    m_new.write_input()


def build_ensemble(pst):
    par_d = "parfiles_fwd_final"
    par_files = [os.path.join(par_d,f) for f in os.listdir(par_d)]
    real_names = [int(f.split('.')[0].split('_')[-1]) for f in par_files]
    pe = pyemu.ParameterEnsemble.from_parfiles(pst=pst,parfile_names=par_files,real_names=real_names)
    print(pst.npar,pe.shape)
    pe.add_base()
    print(pst.npar,pe.shape)
    pe = pe.fillna(0.0)

    return pe


def build_sdz_ensemble():
    fac = -864.0
    pst = pyemu.Pst(os.path.join("sdz","sdz.pst"))
    pe = build_ensemble(pst)
    par = pst.parameter_data
    wf_pars = par.loc[par.parnme.apply(lambda x: x.startswith("wf")),"parnme"]
    base_df = pe.copy()
    base_df.index = base_df.index.map(lambda x: "base_{0}".format(x))
    dfs = [base_df]
    for wf_par in wf_pars:
        df = pe.copy()
        df.index = df.index.map(lambda x: "{0}_{1}".format(wf_par,x))
        df.loc[:,wf_par] = fac

        dfs.append(df)

    df = pd.concat(dfs)
    print(df.shape)
    pe = pyemu.ParameterEnsemble.from_dataframe(pst=pst,df=df)
    pe.to_binary("HPM_sdz.jcb")
    #df.to_csv("HPM_sdz.csv")

def run_sdz_sweep():
    m_d = "sdz_master_sweep"

    pst = pyemu.Pst(os.path.join("sdz","sdz.pst"))
    pst.pestpp_options["sweep_parameter_csv_file"] = os.path.join("..","HPM_sdz.csv")
    pst.write(os.path.join("sdz","sdz.pst"))
    pyemu.helpers.start_slaves("sdz","sweep","sdz.pst",slave_root='.',num_slaves=10,master_dir=m_d,port=4005)


def run_m3_sweep():
    m_d = "m3_master_sweep"

    pst = pyemu.Pst(os.path.join("m3","m3.pst"))
    pst.pestpp_options["sweep_parameter_csv_file"] = os.path.join("..","HPM.csv")
    pst.write(os.path.join("m3","m3.pst"))
    pyemu.helpers.start_slaves("m3","sweep","m3.pst",slave_root='.',num_slaves=8,master_dir=m_d,port=4005)


def check_sdz_ensemble():
    df = pd.read_csv("HPM_sdz.csv",index_col=0)
    df = df.loc[:,df.columns.map(lambda x: x.startswith('wf'))]
    print(df.shape)
    print(df.max())
    for col in df.columns:
        s = df.loc[:,col]

        print(s.value_counts())

        print(col,s.loc[s==s.min()].index)


def plot_sdz():
    pst = pyemu.Pst(os.path.join("sdz","sdz.pst"))
    df = pd.read_csv(os.path.join("sdz_master_sweep","sweep_out.csv"))
    df.columns = df.columns.map(str.lower)
    df.index = df.input_run_id
    sites = pst.observation_data.obsnme.apply(lambda x: '_'.join(x.split('_')[:-1])+'_')
    usites = sites.unique()
    #print(len(usites))


    df = df.loc[:,pst.obs_names]
    df.loc[:, "case"] = df.index.map(lambda x: '_'.join(x.split('_')[:-1]))
    ucases = df.case.unique()
    ax_count,fig_count = 0,0
    nrow,ncol = 5,2
    fig = plt.figure(figsize=(8.5,11))
    ax = plt.subplot(nrow,ncol,1)
    with PdfPages("sdz.pdf") as pdf:
        for icase,ucase in enumerate(ucases):
            udf = df.loc[df.case==ucase,:]
            for usite in usites:

                uudf = df.loc[:,[c for c in udf.columns if c.startswith(usite)]]
                mx = uudf.max().max()

                if mx == 0.0 or len(usite.split('_')) > 3:
                    continue
                print(ucase, usite, mx)
                times = [int(c.split('_')[-1]) for c in uudf.columns]
                ax_count += 1
                if ax_count == nrow * ncol:
                    plt.tight_layout()
                    pdf.savefig()
                    plt.close(fig)
                    fig = plt.figure(figsize=(8.5, 11))
                    ax = plt.subplot(nrow, ncol, 1)
                    ax_count = 1
                    fig_count += 1
                else:
                    ax = plt.subplot(nrow, ncol, ax_count)
                for i in range(uudf.shape[0]):
                    ax.plot(times,uudf.iloc[i,:])
                ax.set_title("case:{0}, site:{1}".format(ucase,usite),loc="left")
                break
            if fig_count > 5:
                break
        plt.tight_layout()
        pdf.savefig()
        plt.close(fig)


if __name__ == "__main__":

    if len(sys.argv) == 1:
        #setup_m3()
        #test("m3")
        #setup_m4()
        #test("m4")
        #test("m4_incr")

        #pst = pyemu.Pst(os.path.join("m3", "m3.pst"))
        #pe = build_ensemble(pst)
        #pe.to_csv("HPM.csv")

        #setup_sdz()
        #check_sdz_ensemble()
        #run_sdz_sweep()
        #run_m3_sweep()
        #test("sdz")
        #test_extract()
        #prep_sdz_wel(org_d,"HPM_M5.nam")

        plot_sdz()

        #build_sdz_ensemble()
    elif len(sys.argv) == 2:
        print("testing scenario ",sys.argv[1])
        test(sys.argv[1])
    else:
        raise Exception("only 1 or 2 cmd args allowed, not:{0}".format(','.join(sys.argv)))
