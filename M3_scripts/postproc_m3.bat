::#!/bin/sh
::set -e
::Misc. cleanup
del HPM035_M3.out
del HPM035_M3_zero.out
del hsim_m3.smp
del hsim_m3_zero.smp
del flow.smp
del flow_comb.smp


::# Postprocessing
mod2obs <head_mod2obs_m3.in 
mod2obs <head_mod2obs_m3_zero.in 
bud2hyd <bud2hyd_M3_zero.in 
bud2hyd <bud2hyd_M3.in 

